var gulp = require("gulp");
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var concat = require("gulp-concat");
var watch = require('gulp-watch');

gulp.task("babel", function () {
    return gulp.src("app/**/*.js")
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(concat("all.js"))
        .pipe(sourcemaps.write(".", {includeContent: true, sourceRoot: '../app'}))
        .pipe(gulp.dest("dist"));
});

gulp.task('default', ['babel']);

gulp.task('watch', function() {
    gulp.watch('app/**/*.js',['babel'])
});

