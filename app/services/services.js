(() => {
    tc.services = function () {
        //<editor-fold desc="propeties">
        let orders;
        let api = 'https://api.myjson.com/bins/3tyc0';
        //</editor-fold>

        //<editor-fold desc="private">
        const getOrders2 = (callback, refresh, filter) => {
            if (orders && !refresh) {
                callback(orders);
            } else {
                $.getJSON('https://api.myjson.com/bins/3tyc0', (data) => {

                    if(filter){
                        //filter by properties
                        data = _.filter(data, function (item) {
                            let tests = [];
                            _.each(filter, (filterItem, filterKey) => {
                                if (!(filterItem instanceof Array)) {
                                    tests.push(item[filterKey] == filterItem);
                                }
                            });

                            return _.every(tests, (item) => { return item;});
                        });

                        //filter drinks
                        if(filter.Items){
                            _.each(data, (order) => {
                                order.Items = _.filter(order.Items, (drink) => {
                                    let tests = [];

                                    _.each(filter.Items[0], (filterItem, filterKey) => {
                                        tests.push(drink[filterKey] == filterItem);
                                    });
                                    return _.every(tests, (item) => { return item;});
                                });
                            });

                            //remove emtpy orders
                            data = _.filter(data, function (order) {
                                return order.Items.length > 0;
                            })
                        }
                    }

                    callback(data);
                });
            }
        };
        //</editor-fold>

        //<editor-fold desc="public">
        const getTeams = (callback) => {
            getOrders2((data) => {
                let teams = _.chain(data)
                    .map((item) => {
                        return item.Team;
                    })
                    .uniq()
                    .value();

                callback(teams);
            })
        };

        const getDrinks = (callback, filter) => {
            getOrders2((data) => {
                let drinks = [];
                data.forEach((order) => {
                    order.Items.forEach(item => {
                        for (var i = 0; i < item.Amount; i++) {
                            drinks.push(item);
                        }
                    });
                });

                drinks = _.groupBy(drinks, 'Name');
                drinks = _.map(drinks, (item, key) => {
                    return {
                        name: key,
                        y: item.length
                    }
                });

                callback(drinks);
            }, true, filter)
        };

        const getDrinkOverTime = (callback, filter) => {

            getOrders2((data) => {
                let drinks = [];
                var today = new Date();
                data.forEach((order) => {
                    order.Items.forEach(item => {
                        for (var i = 0; i < item.Amount; i++) {
                            var time = new Date(today.getFullYear(),
                                today.getMonth(),
                                today.getDay(),
                                +order.Start.split(':')[0],
                                +order.Start.split(':')[1],
                                +order.Start.split(':')[2],0);

                            item.time = time.getTime();
                            drinks.push(item);
                        }
                    });
                });

                drinks = _.groupBy(drinks, 'time');
                drinks = _.sortBy(drinks, 'time');
                drinks = _.map(drinks, (item, key) => {
                    return [
                        item[0].time,
                        item.length
                    ]
                });

                callback(drinks);
            }, true, filter)
        };

        return {
            getDrinks: getDrinks,
            getTeams:getTeams,
            getDrinkOverTime: getDrinkOverTime
        };
        //</editor-fold>
    };

    tc.services = tc.services();
})();

