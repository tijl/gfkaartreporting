(() => {

    tc.charts.area = (container, series) => {
        let chart = {
            title: {
                text: 'Alcohol over time'
            },
            xAxis: {
                type: 'datetime',
                tickInterval: 3600 * 1000
            },
            yAxis: {
                title: '',
                tickInterval: 2
            },
            legend: {
                enabled: false
            },
            tooltip:{
                enabled: false
            },
            credits:{
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    threshold: null
                }
            },
            series: [{
                type: 'area',
                name: 'USD to EUR',
                data: series
            }]
        };

        $(container).highcharts(chart);
    };

})();
