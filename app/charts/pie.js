(() => {

    tc.charts.pie = (container, series) => {
        let pie = {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            legend: {
                verticalAlign: 'top',
                y: 30
            },
            title: {
                text: 'Beverage shares'
            },
            tooltip: {
                enabled: false,
                followTouchMove: false
            },
            plotOptions: {
                plotOptions: {
                    series: {
                        enableMouseTracking: false

                    }
                },
                pie: {
                    allowPointSelect: false,
                    showInLegend: true,
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            series: series
        };

        $(container).highcharts(pie);
    };

})();
