$(() => {

    var getData = (filter) => {
        tc.services.getDrinks((data) => {
            tc.charts.pie('#container', [{
                    name: 'Drinks',
                    colorByPoint: true,
                    data: data
                }]
            );
        }, filter);

        tc.services.getDrinkOverTime((data) => {
            tc.charts.area('#container2', data);
        }, filter || {
                Items: [
                    {
                        Category: "Alcohol"
                    }
                ]
            });
    };

    var dropdownStuff = () => {
        tc.services.getTeams((data) => {
            let teamsElement = $("#teams");

            //options
            let view = {
                teams: data
            };
            let output = Mustache.render('{{#teams}} <option>{{.}}</option>{{/teams}}', view);
            teamsElement.html(teamsElement.html() + output);

            //change event
            teamsElement.on('change', (event) => {
                var value = event.currentTarget.value;
                getData({
                    Team: value
                });
            });
        });
    };

    //init
    (() => {
        getData();
        dropdownStuff();
    })();

});
